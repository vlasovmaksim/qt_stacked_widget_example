#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include "QStackedWidget"
#include "form.hpp"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

public slots:
  void setCurrentIndex(int);

private:
  Ui::MainWindow *ui;

  QStackedWidget *stackedWidget;

  Form * form;
};

#endif // MAINWINDOW_HPP
