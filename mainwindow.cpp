#include "mainwindow.hpp"
#include "ui_mainwindow.h"


#include "QVBoxLayout"
#include "QPushButton"
#include "QVBoxLayout"
#include "QDebug"
#include "QComboBox"


MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  QWidget * rootPageWidget = new QWidget;

  QWidget * firstPageWidget = new QWidget;
  QWidget * secondPageWidget = new QWidget;
  QWidget * thirdPageWidget = new QWidget;

  QPushButton * button_1 = new QPushButton("button1");
  QPushButton * button_2 = new QPushButton("button2");
  QPushButton * button_3 = new QPushButton("button3");

  QVBoxLayout * layoutRootPageWidget = new QVBoxLayout(rootPageWidget);

  QVBoxLayout * layoutFirstPage = new QVBoxLayout(firstPageWidget);
  QVBoxLayout * layoutSecondPage = new QVBoxLayout(secondPageWidget);
  QVBoxLayout * layoutThirdPage = new QVBoxLayout(thirdPageWidget);

  layoutFirstPage->addWidget(button_1);
  firstPageWidget->setLayout(layoutFirstPage);
  firstPageWidget->adjustSize();

  layoutSecondPage->addWidget(button_2);
  firstPageWidget->setLayout(layoutFirstPage);
  firstPageWidget->adjustSize();

  layoutThirdPage->addWidget(button_3);
  firstPageWidget->setLayout(layoutFirstPage);
  firstPageWidget->adjustSize();

  stackedWidget = new QStackedWidget;
  stackedWidget->addWidget(firstPageWidget);
  stackedWidget->addWidget(secondPageWidget);
  stackedWidget->addWidget(thirdPageWidget);

  QComboBox * pageComboBox = new QComboBox(this);
  pageComboBox->addItem(tr("Page 1"));
  pageComboBox->addItem(tr("Page 2"));
  pageComboBox->addItem(tr("Page 3"));

  connect(pageComboBox, SIGNAL(activated(int)),
          this, SLOT(setCurrentIndex(int)));

  layoutRootPageWidget->addWidget(stackedWidget);
  layoutRootPageWidget->addWidget(pageComboBox);

  setCentralWidget(rootPageWidget);

  stackedWidget->setCurrentIndex(0);

  // Or like this below.
  //  stackedWidget->setCurrentWidget(firstPageWidget);
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::setCurrentIndex(int value)
{
    qDebug() << value;

    stackedWidget->setCurrentIndex(value);
}
