#-------------------------------------------------
#
# Project created by QtCreator 2016-11-19T11:06:42
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QStackedWidget
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    form.cpp

HEADERS  += mainwindow.hpp \
    form.hpp

FORMS    += mainwindow.ui \
    form.ui
